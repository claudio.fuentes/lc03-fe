import './App.css';
import Login from "./Component/Login";

function App() {
  return (
    <div className="App">
      <div className="App-header">
      </div>
      <div>
        <div className='container mt-4'>
          <div className='row justify-content-center'>
            <div className='col-3'>
            <Login></Login>
            </div>
          </div>
        </div>
      </div>
      <div className='App-footer mt-5'>
        <div className='container'>
          <div className='row'>
            <div className='col-6'>
              Wake App Aplicaciones
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
