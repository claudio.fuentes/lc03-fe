import React, {Component,Fragment} from 'react'
const axios = require('axios')

export default class Login extends Component{
    constructor(){
        super()
        this.state = {
            user : "",
            pass : ""
        }
        this.handleInput = this.handleInput.bind(this) 
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    handleInput(e){
        const {value,name} = e.target
        this.setState({
            [name]:value
        })
         console.log(this.state);   
    }
    handleSubmit(e){
        axios.post('http://localhost:3012/user/val',this.state).then(done =>{
            console.log(`éxito: ${done}`);
        }).catch(err => {
            console.log(`error: ${err}`);
        })
    }
    render(){
        return(
            <Fragment>
                <div className='card'>
                    <div className='card-header'>
                        <h1>
                            Listo App
                        </h1>
                        <p>login de usuario</p>
                    </div>
                    <div className='card-body'>
                        <form className='formgroup mt-4' onSubmit={this.handleSubmit}>
                            <div className='mt-3'>
                                <label htmlFor="txtUser" className='form-label'>Usuario</label>
                                <input type="text" id="txtUser" name='user' className='form-control' required onChange={this.handleInput}></input>    
                            </div>
                            <div className='mt-3'>
                                <label htmlFor="txtPass" className='form-label'>Contraseña</label>
                                <input type="password" id="txtPass" name='pass' className='form-control' required onChange={this.handleInput}></input>
                            </div>
                            <div className='mt-3 mb-4'>
                                <input type="submit" value="Acceder" className='btn btn-success'></input>
                            </div>
                        </form>
                    </div>
                    <div className='card-footer'>
                        
                    </div>
                </div>
            </Fragment>
        )
    }
}